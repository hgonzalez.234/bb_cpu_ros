bb_cpu_ros
===========
A simple bb camera driver which only use CPU and only publish left and right raw images and its camera info.

# Usage:
1. git the packge into your working space

    ```
    cd catkin_ws/src
    git clone https://gitlab.com/hgonzalez.234/bb_cpu_ros
    cd ..
    catkin_make
    ```
2. Do a calibration yourself:
    
    This option is suggested. Reference: http://wiki.ros.org/camera_calibration
    ```
    roslaunch bb_cpu_ros camera_calibration.launch
    ```
    After calibration:
    Find the left.yaml and right.yaml in the tar file and put them into the bb_cpu_ros/config folder.
    The calibration file will be loaded if you turn <load_bb_config> off in the launch file.

3. launch the code
    ```
    roslaunch bb_cpu_ros bb_cpu_ros.launch
    ```
## Launch file parameters

 Parameter                    |           Description                                       |              Value          
------------------------------|-------------------------------------------------------------|-------------------------           
 resolution                   | Blackbird Camera resolution                                 | VGA                   
 frame_rate                   | Rate at which images are published                          | int                                             
 left_frame_id                | Left Frame ID                                               | string        
 right_frame_id               | Right Frame ID                                              | string        
 load_bb_config               | Whether to use BB calibration file                          | bool        
 config_file_location         | The location of BB calibration file                         | string        
 show_image                   | Whether to use opencv show image                            | bool        

# TODO:

1. add the launch file for stereo_proc.
2. add the nodelet functionality.

# Transcend Robotics:
Patented articulated traction control ARTI technology for stair climbing and obstacle traversal without complex software or controls
http://transcendrobotics.com/

# Author:
Di Zeng

# Adaptation:
Hernán Gonzalez 
